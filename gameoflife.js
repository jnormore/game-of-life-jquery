var timeoutId = 0;

function start(nx, ny) {
  var blockWidth = Math.floor(500 / nx);
  var blockHeight = Math.floor(500 / ny);
  stop();
  initGrid(500, 500, blockWidth, blockHeight);
  tick(0);
}

function stop() {
  clearTimeout(timeoutId);
}

function blockHtml(x, y, w, h, isOn) {
  return "<div id='x-" + x + "-y-" + y + "' class='block " + (isOn ? "block-off" : "block-on") + "' style='width: " + 
                  w + "px; height: " + h + "px;'></div>";
}

function blockElement(x, y) {
  return $("#x-" + x + "-y-" + y);
}

function isBlockOn(block) {
  return (block && $(block).hasClass("block-on"));
}

function neighbourCount(x, y, Nx, Ny) {
  var count = 0;
  if(y < Ny-1 && isBlockOn(blockElement(x, y+1))) count++;
  if(y > 0 && isBlockOn(blockElement(x, y-1))) count++;
  if(x < Nx-1 && isBlockOn(blockElement(x+1, y))) count++;
  if(x > 0 && isBlockOn(blockElement(x-1, y))) count++;
  if(x < Nx-1 && y < Ny-1 && isBlockOn(blockElement(x+1, y+1))) count++;
  if(x < Nx-1 && y > 0 && isBlockOn(blockElement(x+1, y-1))) count++;
  if(x > 0 && y > 0 && isBlockOn(blockElement(x-1, y-1))) count++;
  if(x > 0 && y < Ny-1 && isBlockOn(blockElement(x-1, y+1))) count++;
  return count;
}

function tick(step) {
  $("#time").html(step);
  var gridWidth = $("#grid").data("width");
  var gridHeight = $("#grid").data("height");
  var blockWidth = $("#grid").data("blockWidth");
  var blockHeight = $("#grid").data("blockHeight");

  var Nx = gridWidth / blockWidth;
  var Ny = gridHeight / blockHeight;

  var tmpGrid = new Array(Nx * Ny);
  for(var y = 0; y < Ny; y++) {
    for(var x = 0; x < Nx; x++) {
      var b = blockElement(x, y);
      var isOn = isBlockOn(b);
      var count = neighbourCount(x, y, Nx, Ny);
      if(isOn) {
        if(count < 2) {
          tmpGrid[y*gridHeight + x] = 0;
        } else if(count == 2 || count == 3) {
          tmpGrid[y*gridHeight + x] = 1;
        } else {
          tmpGrid[y*gridHeight + x] = 0;
        }
      } else {
        if(count == 3) {
          tmpGrid[y*gridHeight + x] = 1;
        } else {
          tmpGrid[y*gridHeight + x] = 0;
        }
      }
    }
  }

  for(var y = 0; y < Ny; y++) {
    for(var x = 0; x < Nx; x++) {
      var b = blockElement(x, y);
      if(tmpGrid[y*gridHeight + x] == 1) {
        $(b).addClass('block-on').removeClass('block-off');
      } else {
        $(b).addClass('block-off').removeClass('block-on');
      }
    }
  }

  if(step <= 1000) {
    timeoutId = setTimeout(function() {tick(step+1)}, 100);
  }
}

function initGrid(gridWidth, gridHeight, blockWidth, blockHeight) {
  $("#grid").data("width", gridWidth);
  $("#grid").data("height", gridHeight);
  $("#grid").data("blockWidth", blockWidth);
  $("#grid").data("blockHeight", blockHeight);
  var html = "";
  for(var y = 0; y < gridHeight / blockHeight; y++) {
    for(var x = 0; x < gridWidth / blockWidth; x++) {
      html += blockHtml(x, y, blockWidth, blockHeight, (Math.random() > 0.5));
    }
  }
  $("#grid").html(html);
}

$(document).ready(function() {
  $("#start").click(function(e) {
    start($("#width").val(), $("#height").val());
  });
  $("#stop").click(function(e) {
    stop();
  });
});
